const { check, validationResult } = require("express-validator");
const { FNIR, LNIR, VEIS, PMBAL6CL } = require("../chat/msgs.ts");

exports.validateSignupRequest = [
  check("firstName").notEmpty().withMessage(FNIR),
  check("lastName").notEmpty().withMessage(LNIR),
  check("lastName"),
  check("email").isEmail().withMessage(VEIS),
  check("password").isLength({ min: 6 }).withMessage(PMBAL6CL),
];

exports.validateSigninRequest = [
  check("email").isEmail().withMessage(VEIS),
  check("password").isLength({ min: 6 }).withMessage(PMBAL6CL),
];

exports.isRequestValidated = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.array().length > 0) {
    return res.status(400).json({ error: errors.array()[0].msg });
  }
  
  next();
};
