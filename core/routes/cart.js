const express = require("express");
const { addItemToCart } = require("../controllers/cart");
const { requireSignin, userMiddleware } = require("../common");
const router = express.Router();

router.post("/user/cart/add", requireSignin, userMiddleware, addItemToCart);

module.exports = router;
