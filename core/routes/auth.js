const express = require("express");
const router = express.Router();
const { signup, signin } = require("../controllers/auth");
const {
  validateSigninRequest,
  validateSignupRequest,
  isRequestValidated,
} = require("../validator/auth");

router.post("/signup", validateSignupRequest, isRequestValidated, signup);
router.post("/signin", validateSigninRequest, isRequestValidated, signin);

module.exports = router;
