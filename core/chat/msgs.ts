const SWO = "😯 Something went wrong";
const UIAR = "User is already registered";
const AIAR = "Admin is already registered";
const UCS = "User created successfully!!! 😀";
const ACS = "Admin created successfully!!! 😎";
const DC = "DB connected 💻";
const IP = "Invalid Password";
const AR = "Authorization required";
const UAD = "User access denied 😠";
const AAD = "Admin access denied 😠";

// validator/auth.js
const FNIR = "firstName is required";
const LNIR = "lastName is required";
const VEIS = "Valid Email is required";
const PMBAL6CL = "Password must be at least 6 character long";

module.exports = {
  SWO,
  UIAR,
  UCS,
  DC,
  IP,
  AIAR,
  ACS,
  AR,
  UAD,
  AAD,
  FNIR,
  LNIR,
  VEIS,
  PMBAL6CL,
};
